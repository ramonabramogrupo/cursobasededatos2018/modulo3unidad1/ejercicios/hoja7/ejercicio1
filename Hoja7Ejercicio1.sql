﻿/*
  hoja 7 - unidad 1 - modulo 3 - ejercicio 1
*/

  -- eliminar la base de datos si existe
  DROP DATABASE IF EXISTS hoja7unidad1modulo3Ejercicio1;

  -- crear la base de datos si no existe
  CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3Ejercicio1;

  -- utilizar la base de datos
  USE hoja7unidad1modulo3Ejercicio1;

  -- eliminar la tabla si existe
  DROP TABLE IF EXISTS empleado;

  -- crear la tabla si no existe ya
  CREATE TABLE IF NOT EXISTS empleado(
    dni char(9),
    PRIMARY KEY (dni)
  );

  -- insertamos unos valores
  INSERT INTO empleado VALUES 
    ('dni1'),
    ('dni2');

  
  -- eliminar la tabla departamento
    DROP TABLE IF EXISTS departamento;

  -- crear la tabla de departamento
    CREATE TABLE IF NOT EXISTS departamento(
      codigo varchar(15),
      PRIMARY KEY (codigo)
      );

    -- insertamos datos en la tabla
    INSERT INTO departamento VALUES
      ('departamento1'),
      ('departamento2');

    -- eliminar la tabla
      DROP TABLE IF EXISTS pertenece;
    
    -- crear la tabla pertenece
      CREATE TABLE IF NOT EXISTS pertenece(
        departamento varchar(15),
        empleado char(9),
        PRIMARY KEY (departamento,empleado),
        CONSTRAINT uniqueEmpleado UNIQUE KEY (empleado),
        CONSTRAINT FKPerteneceEmpleado FOREIGN KEY (empleado)
          REFERENCES empleado(dni) on DELETE CASCADE ON UPDATE CASCADE,
        CONSTRAINT FKPerteneceDepartamento FOREIGN KEY (departamento)
          REFERENCES departamento(codigo) ON DELETE CASCADE ON UPDATE CASCADE
        );

      -- tabla proyecto

        DROP TABLE IF EXISTS proyecto;

        CREATE TABLE IF NOT EXISTS proyecto(
          codigoProyecto varchar(15),
          PRIMARY KEY (codigoProyecto)
          );

        -- tabla trabaja
        DROP TABLE IF EXISTS trabaja;

        CREATE TABLE IF NOT EXISTS trabaja(
          empleado char(9),
          proyecto varchar(15),
          fecha date,
          PRIMARY KEY (empleado,proyecto),
          CONSTRAINT FKTrabajaEmpleado FOREIGN KEY (empleado) 
            REFERENCES empleado(dni) 
            ON DELETE CASCADE on UPDATE CASCADE,
          CONSTRAINT FKTrabajaProyecto FOREIGN KEY (proyecto)
            REFERENCES proyecto(codigoProyecto)
            ON DELETE CASCADE on UPDATE CASCADE
          );





